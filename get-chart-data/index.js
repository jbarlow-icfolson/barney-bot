'use strict';

const request = require('request-promise-native');
const services = require('../core/services');

module.exports = function (context, timer) {
  context.log('JavaScript cron trigger function processed a request.');

  services.kegbotAPI()
    .then( data => {
      const recordStartDate = new Date();
      const recordDate = (recordStartDate.getMonth()+1).toString() + recordStartDate.getDate().toString() + recordStartDate.getFullYear().toString();
      const recordTime = recordStartDate.getHours().toString();
      const kegs = data.objects.filter(data => data.online);
      let entries = new Array();

      kegs.map(keg => {
        let entry = new Object();
        
        entry.displayName = keg.beverage.name;
        entry.name = keg.beverage.name.replace(' ', '') + keg.beverage.id;
        entry.level = (Math.round( keg.percent_full * 10 ) / 10).toString();
        entry.date = recordDate.toString();
        entry.time = recordTime;

        entries.push(entry);
      });

      services.barneyDB.getByDate(recordDate)
        .then(data => {
          let haveData = data.length > 0 ? true : false,
              entryExist = false;

          if (haveData) {
            for (var i = 0, l = data.length; i < l; i++) {
              if (recordTime == data[i].time-4) {
                entryExist = true;
                break;
              }
            }
          }

          if (haveData && !entryExist) {
            entries.map((entry, idx) => {
              services.barneyDB.createEntry(entry, context)
                .then((data) => {
                  context.log('Database updated successfully.');

                  if (entries.length === idx+1) {
                    context.done();
                  }
                })
                .catch( err => {
                  context.log('The database update failed with the following error:', err);

                  if (entries.length === idx+1) {
                    context.done();
                  }
                });
            });
          } else {
            context.log('Entry aleady exists in the database.');
            context.done();
          }
        })
        .catch(err => {
          context.log('An error has occured getting the data from the database with the following error:', err);
          context.done();
        });
    })
    .catch(err => {
      context.log('An error has occured getting the data from kegbot with the following error:', err);
      context.done();
    });
};