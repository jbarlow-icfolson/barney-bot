#Barney Bot


##Requirements
 - [Node 5.4.1](https://nodejs.org/download/release/v5.4.1/)
 - [NPM 3.3.12](https://www.npmjs.com/)


##Setup
 - Repo Link: `https://jbarlow-icfolson@bitbucket.org/jbarlow-icfolson/barney-bot.git`
 - Run `npm start`


##Structure
 - barney-chart
    - This app integrates in to slack and it returns the the consumption rate of each keg via a graphical chart.
 - get-chart-data
    - This app collects the consumption data from the kegbot and stores it in a database.
 - core
    - Contains shared methods used across all the apps.
 - core/services
    - Contains shared services used across all the apps.


##Testing
 - get-chart-data
    - `node utils/get-chart-data.js`
    - Tests the api call to collect the consumption data locally.
 - barney-chart
    - `node utils/barney-chart.js`
    - Tests the chartting functionality.