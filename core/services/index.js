const barneyDB = require('./barneyDB');
const kegbotAPI = require('./kegbotAPI');

module.exports = { 
  barneyDB,
  kegbotAPI
};