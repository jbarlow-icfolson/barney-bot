'use strict';

const request = require('request-promise-native');

const createEntry = arr => {
  return request({
    uri: process.env.BARNEY_API_URL + '/daily-consumption',
    method: 'POST',
    body: arr,
    json: true
  });
};

const getByDate = arr => {
  return request({
    uri: process.env.BARNEY_API_URL + '/daily-consumption/by-date/' + arr,
    method: 'GET',
    json: true
  });
}

module.exports = {
  createEntry,
  getByDate
};