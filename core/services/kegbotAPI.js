'use strict';

const request = require('request-promise-native');

module.exports = (arr) => {
  return request({
    uri: process.env.KEGBOT_SERVER_URL + '/api/kegs',
    qs: {api_key: process.env.KEGBOT_API_KEY},
    headers: {'User-Agent': 'Request-Promise'},
    json: true
  });
};