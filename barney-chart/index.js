'use strict';

const services = require('../core/services');
const quiche = require('quiche');

module.exports = function (context, req) {
  context.log('JavaScript HTTP trigger function processed a request.');

  const params = req.body.split('&'),
        newDate = new Date(),
        recordDate = (newDate.getMonth()+1).toString() + newDate.getDate().toString() + newDate.getFullYear().toString();

  services.barneyDB.getByDate(recordDate)
    .then( data => {
      if (data.length > 0) {
        let beerNames = new Array(),
            filteredBeers = new Array();

        // get array of unique beer names from returned data
        data.map(beer => {
          beerNames[beer.name] = {'displayName': beer.displayName};
        });

        // filter list of returned beers by name
        for (var key in beerNames) {
          filteredBeers[key] = data.filter(beer => {
            if (beer.name === key) {
              return beer;
            }
          });
        }

        // create base objects for the chart
        let quiche = require('quiche'),
            chart = quiche('line');

        // create data points for chart y axis
        for (var key in beerNames) {
          let dataPoints = new Array();

          filteredBeers[key].map(beer => {
            dataPoints.push(beer.level);
          });

          var color = (function co(lor){return (lor += [0,1,2,3,4,5,6,7,8,9,'a','b','c','d','e','f'][Math.floor(Math.random()*16)]) && (lor.length == 6) ?  lor : co(lor);})('');

          chart.addData(dataPoints, beerNames[key].displayName, color);
        }

        // create data points for chart x axis
        let xAxisDataPoints = new Array();

        filteredBeers[Object.keys(beerNames)[0]].map(beer => {
          xAxisDataPoints.push(beer.time-4);
        });

        chart.addAxisLabels('x', xAxisDataPoints);

        // configure chart
        chart.setTitle('Rate of Consumption');
        chart.setAutoScaling();
        chart.setLegendBottom();

        // generate the chart URL
        let imageUrl = chart.getUrl(true);

        context.res = {
          "text": "Here's the _BUUUUUURRRRRPPPPPPP_, chart!",
          "attachments": [
            {
              "fallback": "Required plain-text summary of the attachment.",
              "color": "#36a64f",
              "image_url": imageUrl
            }
          ]
        }
      } else {
        context.res = {
          status: 400,
          text: "Oops, guess I got too drunk and now I can't find the data."
        };
      }

      context.done();
    })
    .catch(err => {
      context.log('An error has occured in barney-chart: ', err);

      context.res = {
        status: 400,
        text: "Oops, guess I got too drunk and screwed up. Maybe try again later."
      };

      context.done();
    });
};