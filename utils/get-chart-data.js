const context = require('./context');
const getKegbotData = require('../get-chart-data');
const appConfig = require('./app.config.json');

process.env.KEGBOT_SERVER_URL = appConfig.KEGBOT_SERVER_URL;
process.env.KEGBOT_API_KEY = appConfig.KEGBOT_API_KEY;
process.env.BARNEY_API_URL = appConfig.BARNEY_API_URL;

const ctx = context();
ctx.then(res => console.log('response', res));

getKegbotData(ctx);