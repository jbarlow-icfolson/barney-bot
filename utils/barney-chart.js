const context = require('./context');
const barneyChart = require('../barney-chart');
const appConfig = require('./app.config.json');

process.env.BARNEY_API_URL = appConfig.BARNEY_API_URL;

const ctx = context();
const req = {
  body: "token=m3XKKia9LtGFcwgVXdiyZ0Ro&team_id=T0001&team_domain=example&channel_id=C2147483705&channel_name=test&timestamp=1355517523.000005&user_id=U2147483697&user_name=Steve&text=googlebot: What is the air-speed velocity of an unladen swallow?&trigger_word=googlebot:"
};

ctx.then(res => console.log('response', res));

barneyChart(ctx, req);